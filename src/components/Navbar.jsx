import { Link, Outlet } from "react-router-dom";
import { Menu } from "antd";
import { useState } from "react";

const { Item } = Menu;

const Navbar = () => {
  const [current, setCurrent] = useState("home");

  const handleCurrent = ({ key }) => setCurrent(key);
  return (
    <>
      <Menu onClick={handleCurrent} selectedKeys={[current]} mode="horizontal">
        <Item key="home">
          <Link to="/">Home</Link>
        </Item>
      </Menu>
      <Outlet />
    </>
  );
};

export default Navbar;
