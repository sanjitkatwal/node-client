import { Route, Routes } from "react-router-dom";
import Navbar from "../components/Navbar";
import NotFound404Page from "../pages/404NotFound";

import ItemsPage from "../pages/items";
import AddItem from "../pages/items/add";
import EditItemPage from "../pages/items/edit";
import ItemPage from "../pages/items/item";

const Paths = () => {
  return (
    <Routes>
      <Route path="/" element={<Navbar />}>
        <Route index element={<ItemsPage />} />
        <Route path=":id" element={<ItemPage />} />
        <Route path="add" element={<AddItem />} />
        <Route path="edit/:id" element={<EditItemPage />} />
      </Route>
      <Route path="*" element={<NotFound404Page />} />
    </Routes>
  );
};

export default Paths;
