import { LoadingOutlined } from "@ant-design/icons";
import { Col, Row, Spin, Card, Alert } from "antd";
import { listItems } from "../../apis/item";
import { Link, Outlet } from "react-router-dom";
import { useQuery } from "react-query";
import React from "react";
const { Meta } = Card;

export const ITEMS = "items";
const ItemsPage = () => {
  const { data, error, isLoading } = useQuery(ITEMS, listItems);

  if (isLoading) {
    return (
      <div>
        <Spin size="large" indicator={<LoadingOutlined spin />} />
      </div>
    );
  }

  if (error) {
    return <Alert message={error} type="error"></Alert>;
  }

  return (
    <Row gutter={[16, 24]}>
      {data.items.map((item) => (
        <Col span={6} key={item._id}>
          <Card
            hoverable
            cover={<img src={item.image} alt={item.name} />}
            bordered={false}
          >
            <Meta
              title={<Link to={`${item._id}`}>{item.name}</Link>}
              description={item.description}
            />
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default ItemsPage;
