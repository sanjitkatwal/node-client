import { LoadingOutlined } from "@ant-design/icons";
import { Alert, Card, Spin } from "antd";
import { getItemById } from "../../apis/item";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { ITEMS } from ".";
const { Meta } = Card;

const ItemPage = () => {
  const { id } = useParams();
  const { data, error, isLoading } = useQuery([ITEMS, id], getItemById(id));

  if (isLoading) {
    return (
      <div>
        <Spin size="large" indicator={<LoadingOutlined spin />} />
      </div>
    );
  }

  if (error) {
    return <Alert message={error} type="error"></Alert>;
  }

  return (
    <Card cover={<img src={data.image} alt={data.name} />}>
      <Meta title={data.name} description={data.description} />
    </Card>
  );
};

export default ItemPage;
