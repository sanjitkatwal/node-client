import {
  Alert,
  Button,
  Form,
  Input,
  InputNumber,
  notification,
  Select,
  Spin,
  Upload,
} from "antd";
import { getItemById, updateItem } from "../../apis/item";
import { uploadImage } from "../../apis/image-upload";
import { listCategories } from "../../apis/category";
import { LoadingOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { ITEMS } from ".";
import { useEffect, useState } from "react";

const { Item, useForm } = Form;
const { Option } = Select;

const fieldRequired = { required: true, message: "Field required" };

export const CATEGORIES = "categories";

const filterOption = (input, { children }) =>
  children.toLowerCase().indexOf(input.toLowerCase()) !== -1;

const filterSort = ({ children: a }, { children: b }) => {
  return a.toLowerCase().localeCompare(b.toLowerCase());
};

const EditItemPage = () => {
  const [form] = useForm();
  const { id } = useParams();

  const { data, error, isLoading } = useQuery([ITEMS, id], getItemById(id));

  const [image, setImage] = useState();
  const [selectedCategory, setSelectedCategory] = useState();

  const [imageUploading, setImageUploading] = useState(false);
  const [formSubmitting, setFormSubmitting] = useState(false);

  const categoryList = useQuery(CATEGORIES, listCategories);

  useEffect(() => {
    if (data) {
      const {
        _id,
        _v,
        tags,
        active,
        image,
        createdAt,
        createdBy,
        category,
        updatedAt,
        updatedBy,
        ...rest
      } = data;

      setSelectedCategory(category);
      setImage(image);
      form.setFieldsValue({ ...rest, category: category._id });
    }
  }, [data, form]);

  const handleSelect = (value) => {
    const category = categoryList.data.categories.find(
      ({ _id }) => _id === value
    );
    setSelectedCategory(category);
  };

  const beforeUpload = (file) => {
    console.log("file is >>", file);
    const isAcceptedFile = /jpeg|jpg|png/.test(file.type.split("/").pop());
    if (!isAcceptedFile) {
      notification.error({ message: `${file.name} is not an image file` });
    }
    return isAcceptedFile;
  };

  const handleUpload = async (value) => {
    console.log("Value is >>>", value);
    const { file } = value;

    try {
      setImageUploading(true);

      const { image, mesage } = await uploadImage(file);

      setImage(image);

      notification.success({ mesage });
    } catch (error) {
      console.log("error is >>", error);
      notification.error({
        message: error.response?.data
          ? error.response?.data.error
          : "Item Updated failed",
      });
    } finally {
      setImageUploading(false);
    }
  };
  const handleSubmit = async (value) => {
    console.log(value);
    try {
      setFormSubmitting(true);

      const { message } = await updateItem(id, { ...value, image });

      notification.success({ message });
    } catch (error) {
      notification.error({
        message: error.response?.data
          ? error.response?.data
          : error.stack ?? "Error",
      });
    } finally {
      setFormSubmitting(false);
    }
  };

  if (isLoading) {
    return (
      <div>
        <Spin size="large" indicator={<LoadingOutlined spin />} />
      </div>
    );
  }

  if (error) {
    return <Alert message={error} type="error"></Alert>;
  }

  return (
    <Form form={form} onFinish={handleSubmit}>
      <Item name="name" label="Item" rules={[fieldRequired]}>
        <Input placeholder="Enter name" />
      </Item>

      <Item name="description" label="description" rules={[fieldRequired]}>
        <Input.TextArea placeholder="Enter Description" />
      </Item>

      <Item name="price" label="price" rules={[{ required: true }]}>
        <InputNumber placeholder="Enter Price" />
      </Item>

      <Upload
        name="image"
        listType="picture-card"
        showUploadList={false}
        beforeUpload={beforeUpload}
        accept="image/*"
        customRequest={handleUpload}
      >
        {image ? (
          <img
            style={{ width: "5rem", height: "5rem" }}
            src={image}
            alt="item-imge"
          />
        ) : (
          "Upload"
        )}
      </Upload>
      <Item name="category" label="Category" rules={[{ required: true }]}>
        <Select
          showSearch
          placeholder="Select a Category"
          loading={categoryList.isLoading}
          filterOption={filterOption}
          filterSort={filterSort}
          onChange={handleSelect}
        >
          {categoryList.data?.categories.map(({ _id, name }) => (
            <Option key={_id} value={_id}>
              {name}
            </Option>
          ))}
        </Select>
      </Item>

      <Item
        name="subCategory"
        label="Sub Category"
        rules={[{ required: true }]}
      >
        <Select
          showSearch
          placeholder="Select a Sub Category"
          loading={categoryList.isLoading}
          filterOption={filterOption}
        >
          {selectedCategory?.subCategories?.map(({ _id, name }) => (
            <Option key={_id} value={_id}>
              {name}
            </Option>
          ))}
        </Select>
      </Item>

      <Button
        htmlType="submit"
        disabled={imageUploading}
        loading={formSubmitting}
      >
        Submit
      </Button>
    </Form>
  );
};

export default EditItemPage;
