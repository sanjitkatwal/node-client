import axios from "../axios";
import { CATEGORIES_URL } from "./urls";

export const listCategories = async () => {
  const {
    data: { categories, total },
  } = await axios.get(CATEGORIES_URL);
  return { categories, total };
};
