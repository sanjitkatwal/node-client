import axios from "../axios";
import { IMAGE_UPLOAD_URL } from "./urls";

export const uploadImage = async (file) => {
  const data = new FormData();
  data.append("image", file);

  const {
    data: { message, image },
  } = await axios.post(IMAGE_UPLOAD_URL, data, { timeout: 20000 });

  return { message, image };
};
