export const ITEMS_URL = "/api/v1/items";

export const CATEGORIES_URL = "api/v1/categories";

export const IMAGE_UPLOAD_URL = "api/v1/uploads/image";
