import $axios from "../axios";
import { ITEMS_URL } from "./urls";

export const listItems = async () => {
  const {
    data: { items, total },
  } = await $axios.get(ITEMS_URL);
  return { items, total };
};

export const getItemById = (id) => async () => {
  const {
    data: { item },
  } = await $axios.get(`${ITEMS_URL}/${id}`);
  return item;
};

export const createItem = async (data) => {
  const {
    data: { message, item },
  } = await $axios.post(ITEMS_URL, data);

  return { message, item };
};

export const updateItem = async (id, data) => {
  const {
    data: { message, item },
  } = await $axios.put(`${ITEMS_URL}/${id}`, data);

  return { message, item };
};
